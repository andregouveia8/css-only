Before upgrading or repairing your theme, you will need to request the new version or download the theme again at BindTuning.com. 

You can find all the instructions to upgrade your theme version <a href="http://support.bindtuning.com/hc/en-us/articles/204447149-How-do-I-request-an-update-for-my-theme-" target="_blank">here</a>.


<p class="alert-warning"> <strong>ALWAYS make a backup</strong> of all of your theme assets, including CSS files, page layouts,...Upgrading or repairing the theme will remove all custom changes you have applied previously.</p>

<p class="alert-warning"> Before upgrading or repairing your theme, clean your browser cache.</p>


<a name="uninstallthetheme"></a>
###1# Uninstall the theme

To upgrade your theme version you need to first completely remove the previous version of the same theme from your website. 

You can find the instructions for uninstalling your theme <a href="http://bindtuning-sharepoint-css-only-themes.readthedocs.io/en/latest/Uninstall/" target="_blank">here</a>.

----

<a name="installthethemeagain"></a>
###2# Install the theme again

After completely removing the theme you can move on to installing the new version.

You can find the instructions for installing your theme <a href="http://bindtuning-sharepoint-css-only-themes.readthedocs.io/en/latest/SETUP/Installation/" target="_blank">here</a>.