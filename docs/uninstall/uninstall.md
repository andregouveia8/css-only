To uninstall your theme, you need to:


1. On the root of your site, open the **Settings** menu and click on **Site Settings**;

2. Under **Site Collection Administration**, click on **Site Collection Features**; 

	<p class="alert-success">**Site Collection Features** will only be visible if you are a Site Collection Administrator</p>

3. Deactivate the **yourthemename** Theme feature; 

4. Go back to **Site Settings**;

5. Under **Web Design Galleries**, click on **Solutions**;

6. Select **yourthemename**.SPCSS.wsp and click on **Deactivate**;

7. Select **yourthemename**.SPCSS.wsp again and click on **Delete**;

Theme uninstalled! ✅