Here, you will find useful instructions to use and format the features included in your theme using what we call "BindTuning settings".

To access BindTuning settings go to **Settings** and click on **BindTuning Settings**. 

<p class="alert-warning">BindTuning Settings will only be available if you are a site collection Owner or Administrator.</p>

<p class="alert-warning">The settings described in this section are not available to all BindTuning themes. To check what if these settings are available to your theme, please go to the <strong>BindTuning Settings</strong> page, inside your SharePoint environment.</p>

###BindTuning Settings

Here is a list of all the BindTuning settings available. Click on the link to read more about a specific setting. 

- [Horizontal menu](#horizontalmenu)
- [Vertical menu](#verticalmenu)
- [Hide Quick Launch](#hidequicklaunch)
- [Text Layouts](#textlayouts)
- [Social Networks](#socialnetworks)
- [Tokens](#tokens)
- [Footer Zones](#footerzones)

----
<a name="horizontalmenu"></a>
**HORIZONTAL MENU**

- **Default Menu**: It displays all nested sub items as drop-downs.

- **Mega Menu**: It's available for menus with 3 levels. It will automatically display a mega menu structure when sub items exist or when you have a regular drop-down sub-navigation (when only one sub level is present).

<a name="verticalmenu"></a>
**VERTICAL MENU**

- **Default Menu**: All menu items are visible when the menu is loaded.

- **Accordion Menu**:  Nested sub-items are hidden by default and they are only visible when the user mouses hover the parent node.

<a name="hidequicklaunch"></a>
**HIDE QUICK LAUNCH**

If you want to have a page with a full width layout, and without the vertical navigation, here is where you can add the page URL.

<a name="textlayouts"></a>
**TEXT LAYOUTS**

SharePoint collaboration sites, like Team Sites, do not include the Page Layout functionality that allow us to add responsive page layouts, instead they include text layouts that are not responsive.
By default, the theme does not change the text layout structure but with this setting you can decide if you want to make your Text Layouts responsive.


<a name="socialnetworks"></a>
**SOCIAL NETWORKS**

To add social networks to your theme's social zone type your social page URL in one of the predefined social networks, included in your theme.

<a name="tokens"></a>
**TOKENS**

Tokens can be added to the footer of the site, as text or links. Although they come with predefined names, you can always change it to any custom name of your choosing.

<a name="footerzones"></a>
**FOOTER ZONES**

The footer zones allow plain text and html. If you want to replicate the same footer of your theme's live preview you can do so by copying the HTML from the **demo content zip file** and paste it in any footer section. 

--------
**To download  your demo content zip file:**
 
1. Access your account at [bindtuning.com](http://bindtuning.com);
2. On the **My Downloads** tab, select the theme; 
3. Under documentation you will find the .zip file.

![downloaddemocontent.png](https://bitbucket.org/repo/g6RLX7/images/279353872-downloaddemocontent.png)