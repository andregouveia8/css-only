<a name="downloadthetheme"></a>
### 1# Download the theme ###
Ready to get started? First you need to download your theme. 

1. Access your account at [BindTuning.com](#http://bindtuning.com/);
2. Go to **My Downloads**, mouse hover the theme and click **View Theme** to open the theme details page;
3. Last but not least, click on **Download**.

![downloadtheme.png](https://bitbucket.org/repo/g6RLX7/images/3813436025-downloadtheme.png)

Ok, on to unzipping the file.


<a name="unzipthefile"></a>
### 2# Unzip the file ###

After unzipping your theme package, you will find a single .wsp file.

-------------


└── *yourthemename*.SPCCSS.wsp 
